# SESAM


The sesam.py file contains the model to produce semi-supervised representation as described in [1] and [2]

The GBSSL.PY file is obtained from the project: https://github.com/junliangma/gbssl

[1] 10.1109/IJCNN.2018.8489353 : Dino Ienco, Ruggero G. Pensa: Semi-Supervised Clustering With Multiresolution Autoencoders. IJCNN 2018: 1-8

[2] 10.1109/TNNLS.2019.2955565 : Dino Ienco, Ruggero G. Pensa: Enhancing Graph-Based Semisupervised Learning via Knowledge-Aware Data Embedding. Accepted for publication @IEEE TNNLS


Example to run the code:

* python sesam.py sonar sonar/labels/0_10.npy

This command will be create a folder under sonar (sonar/embeddings) and file named 0_10.npy, then you can run the graph-based semi-supervised learning algorithm considering the new representation via the command:

* python classify.py sonar sonar/embeddings/0_10.npy sonar/labels/0_10.npy

This command will return the result of the classification procedure.

In the sonar repository the data.npy file contains the data while the class.npy file contains the associated class information


To execute the code you need the following python libraries:
* Scikit-learn
* KERAS
* Tensorflow
* Scipy
* Numpy


